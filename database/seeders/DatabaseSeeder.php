<?php

namespace Database\Seeders;

use App\Models\Like;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\User::factory()
            ->count(10)
            ->has(
                \App\Models\Photo::factory()->count(10)
            )
            ->create();

        for($i = 0; $i < 10; $i++) {
            $user = \App\Models\User::find(rand(1, 10));
            $user->followers()->attach(\App\Models\User::find(rand(1, 10)));
        }

        $this->call(LikeTableSeeder::class);
        $this->call(CommentTableSeeder::class);

    }
}
